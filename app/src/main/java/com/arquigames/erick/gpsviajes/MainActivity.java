package com.arquigames.erick.gpsviajes;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.arquigames.erick.gpsviajes.dummy.DummyContent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.UUID;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, RoutesTrackedFragment.OnListFragmentInteractionListener {

    private GoogleMap mMap;
    private float mMapZoomLevel = 1;
    private MarkerOptions currMarker;
    private String fabText = "Replace with your own action";
    private RoutesTrackedFragment routesTrackedFragment;
    public final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0x00001;
    public final int MY_PERMISSIONS_REQUEST_READ_ACCESS_FINE_LOCATION = 0x00002;

    private String phone_imei = null;

    private TextView nameView = null;
    private TextView textView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.showFooterSnackBar(view);
            }
        });
        */

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);


        nameView = headerView.findViewById(R.id.nameView);
        textView = headerView.findViewById(R.id.textView);

        Intent intent = this.getIntent();
        textView.setText(intent.getStringExtra("email"));
        nameView.setText(intent.getStringExtra("name"));


        startMapFragment();
        this.checkPermissions();

    }
    @Override
    public void onResume() {
        super.onResume();
    }


    private void enableLocationService() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                parseLocationData(location);
                fabText = location.toString();
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                fabText = "provider changed: " + provider + ", status: " + status;
            }

            public void onProviderEnabled(String provider) {
                fabText = "provider enabled: " + provider;
            }

            public void onProviderDisabled(String provider) {
                fabText = "provider disabled: " + provider;
            }
        };
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 1, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, locationListener);
    }
    private void startMapFragment() {
        Toast toast;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);




        boolean cantPassPermissionLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
        if (!cantPassPermissionLocation) {
            this.enableLocationService();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                toast = Toast.makeText(this, "access fine location required", Toast.LENGTH_SHORT);
                toast.show();

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_READ_ACCESS_FINE_LOCATION);
            }
        }

    }

    private void getPhoneInfo() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Toast toast;
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                phone_imei = tm.getImei();
            }else{
                phone_imei = tm.getDeviceId();
            }
            toast = Toast.makeText(this,"IMEI "+phone_imei,Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    private void checkPermissions(){
        Toast toast;
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_PHONE_STATE)==PackageManager.PERMISSION_GRANTED;
        if(granted){
            this.getPhoneInfo();
        }else{
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_PHONE_STATE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                toast = Toast.makeText(this,"Phone state required",Toast.LENGTH_SHORT);
                toast.show();

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            }
        }

    }

    private void showFooterSnackBar(View view) {
        Snackbar.make(view, fabText, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_routes) {
            // Handle the camera action
            this.startRoutesActivity();
        } else if (id == R.id.nav_vehicles) {
            this.startVehiclesActivity();

        } else if (id == R.id.nav_devices) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        item.setChecked(false);
        return true;
    }

    private void startRoutesActivity() {
        Intent intent = new Intent(MainActivity.this, RoutesFullscreenActivity.class);
        startActivity(intent);

    }

    private void startVehiclesActivity() {
        Intent intent = new Intent(MainActivity.this, VehiclesFullscreenActivity.class);
        startActivity(intent);
    }

    private void loadRoutesTrackedFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        routesTrackedFragment = new RoutesTrackedFragment();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.map, routesTrackedFragment);
        transaction.commit();
        transaction.addToBackStack(null);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMapZoomLevel = mMap.getCameraPosition().zoom;
    }
    private void parseLocationData(Location loc){
        if(mMap!=null){
            DummyContent.DummyItem item = new DummyContent.DummyItem(UUID.randomUUID().toString(),loc.getLatitude(),loc.getLongitude());
            if(routesTrackedFragment!=null){
                routesTrackedFragment.addDummyItem(item);
            }
            LatLng pos = new LatLng(loc.getLatitude(), loc.getLongitude());
            if(currMarker==null){
                currMarker = new MarkerOptions().position(pos).title("Current Location");
                mMap.addMarker(currMarker);
            }else{
                currMarker.position(pos);
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLng(pos));

        }
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {
        this.fabText = item.uuid;
    }

    public void zoomInMap(View view) {
        if(mMap!=null){
            mMapZoomLevel++;
            mMap.animateCamera(CameraUpdateFactory.zoomTo(mMapZoomLevel));
        }
    }

    public void zoomOutMap(View view) {
        if(mMap!=null){
            mMapZoomLevel--;
            mMap.animateCamera(CameraUpdateFactory.zoomTo(mMapZoomLevel));
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {

        Toast toast;
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.getPhoneInfo();

                } else {
                    toast = Toast.makeText(this, "Phone state cant granted", Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.enableLocationService();

                } else {
                    toast = Toast.makeText(this, "access fine location cant granted", Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
