package com.arquigames.erick.gpsviajes.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    private static final int COUNT = 2;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(UUID.randomUUID().toString()));
        }
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.uuid, item);
    }

    private static DummyItem createDummyItem(String uuid) {
        return new DummyItem(uuid,0,0);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String uuid;
        public final double latitude;
        public final double longitude;

        public DummyItem(String uuid, double latitude, double longitude) {
            this.uuid = uuid;
            this.longitude = longitude;
            this.latitude = latitude;
        }

        @Override
        public String toString() {
            return uuid+"[ "+latitude+","+longitude+" ]";
        }
    }
}
